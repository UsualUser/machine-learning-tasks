Link:https://www.kaggle.com/spscientist/students-performance-in-exams

Context

 This is fictional data. The dataset is not from a real context, and the dataset does not represent real people.

Content

This data set consists of the marks secured by the students in various subjects.

Inspiration

To understand the influence of the parents background, test preparation etc on students performance

Tasks

1) Regression: Predict results of exams
2) Classification: Predict gender of student

Data:
gender:{female,male} - student's gender
race/ethnicity:{group A,group B,group C,group D,group E} - abstract race of student
parental level of education:{some high school,high school,some college,associate's degree,bachelor's degree,master's degree} - student's parental level of education
lunch:{standard,free/reduced} - which type of lunch student eat
test preparation course:{none, completed} - none : student uncomplete preparation course; complete : student complete preparation course
math score:[0,100] - student's mark for math exam
reading score:[0,100] - student's mark for reading exam
writing score:[0,100] - student's mark for writing exam